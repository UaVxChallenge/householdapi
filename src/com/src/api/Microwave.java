package com.src.api;

public class Microwave {

	public static boolean isOpen = false;
	
	public static void MicrowaveOpen(Boolean Open){
		
		if(Open == true){
			isOpen = true;
			System.out.println("Microwave is now open!");
		}
		
		if(Open == false){
			isOpen = false;
			System.out.println("Microwave is still closed...");
			
		}
		
	}
	
	public static void putInMicrowave(String Item){
		
		if(isOpen == true){
			
			System.out.println("You put a " + Item + " in the microwave!");
			
		}else if(isOpen == false){
			
			System.out.println("You can't put something into a closed microwave!");
			
			return;
		}
		
	}
	
}
