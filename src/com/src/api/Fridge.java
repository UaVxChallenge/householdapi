package com.src.api;

public class Fridge {
	
	public static String item;
	public static boolean fridgeOpen = false;

	public static void itemInFridge(String Item){
		
		item = Item;
		
	}
	
	public static void openFridge(Boolean isOpen){
		
		fridgeOpen = isOpen;
		
		System.out.println("The fridge is now open!");
		
	}
	
	public static void getItem(String Item){
		
		Item = item;
		
		if(fridgeOpen == true){
			
			System.out.println("You have gotten out a(n) " + Item );
			
			return;
		}else if(fridgeOpen == false){
			
			System.out.println("The fridge is closed! You must open it first!");			
			
			return;
		}
		
	}
	
	
	
}
